provider "aws" {
  region = "${var.aws_region}"
}

terraform {
  backend "s3" {}
}

module "vpc" {
  source = "git::ssh://git@gitlab.com/terragrunt-techtalk/terraform-modules/vpc.git?ref=0.1.0"

  vpc_name           = "${var.environment}-${var.vpc_name}"
  vpc_cidr_block     = "${var.vpc_cidr_block}"
  subnet_cidr_blocks = ["${var.subnet_cidr_blocks}"]

  tags = "${var.common_tags}"
}
