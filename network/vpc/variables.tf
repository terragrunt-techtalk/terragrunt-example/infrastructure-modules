variable "aws_region" {
  description = "Região da AWS"
}

variable "environment" {
  description = "Sigla de identificação do ambiente"
}

variable "vpc_name" {
  description = "Nome da VPC"
}

variable "common_tags" {
  description = "Tags para recursos da AWS"
  type        = "map"
}

variable "vpc_cidr_block" {
  description = "Bloco de rede para VPC"
  default     = "10.10.0.0/16"
}

variable "subnet_cidr_blocks" {
  description = "Blocos de rede para as subnets a serem criadas"
  default     = ["10.10.1.0/24", "10.10.2.0/24"]
}
