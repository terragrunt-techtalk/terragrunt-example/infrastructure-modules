variable "aws_region" {
  description = "Região da AWS"
}

variable "vpc_name" {
  description = "Nome da VPC onde será criada a instância EC2"
}

variable "environment" {
  description = "Sigla de identificação do ambiente"
}

variable "common_tags" {
  description = "Tags para recursos dos objetos da AWS"
  type        = "map"
}

variable "instance_type" {
  description = "Tipo da instância EC2"
}

variable "git_repo" {
  description = "Repositório com o código do site"
}

variable "git_version" {
  description = "Versão do site"
  default     = "master"
}
