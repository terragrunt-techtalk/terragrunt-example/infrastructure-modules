provider "aws" {
  region = "${var.aws_region}"
}

terraform {
  backend "s3" {}
}

data "aws_vpc" "selected" {
  tags = {
    Name = "${var.environment}-${var.vpc_name}"
  }
}

data "aws_subnet_ids" "public" {
  vpc_id = "${data.aws_vpc.selected.id}"

  tags = {
    Type = "public"
  }
}

module "web_server" {
  source = "git::ssh://git@gitlab.com/terragrunt-techtalk/terraform-modules/ec2.git?ref=0.1.0"

  name                        = "${var.environment}-webserver"
  subnet_ids                  = ["${data.aws_subnet_ids.public.ids}"]
  user_data                   = "${data.template_file.user_data.rendered}"
  associate_public_ip_address = "true"
  instance_type               = "${var.instance_type}"

  tags = "${var.common_tags}"
}

resource "aws_security_group_rule" "allow_http" {
  type        = "ingress"
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${module.web_server.security_group_id}"
}

data "template_file" "user_data" {
  template = "${file("user_data.sh")}"

  vars = {
    git_repo    = "${var.git_repo}"
    git_version = "${var.git_version}"
  }
}
