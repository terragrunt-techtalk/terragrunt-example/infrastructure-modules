#!/bin/bash

# Atualizar pacotes do sistema
yum update -y

# Instalar repositorio epel
yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

# Instalar pacotes para o servidor
yum install -y nginx git

# Copiar site web
cd /usr/share/nginx/html
rm -rf *
git clone --branch ${git_version} ${git_repo} ./
rm -rf .git

# Inicializar o servico
systemctl start nginx
